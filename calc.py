
def sumar(x, y):
    return x + y


def restar(x, y):
    return x - y


if __name__ == "__main__":
    suma = sumar(1, 2)
    print(suma)
    suma = sumar(3, 4)
    print(suma)
    resta = restar(6, 5)
    print(resta)
    resta = restar(8, 7)
    print(resta)
